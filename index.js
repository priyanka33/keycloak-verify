"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _ramda = require("ramda");

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _jwkToPem = _interopRequireDefault(require("jwk-to-pem"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var cache = {};

var verifyOnline = function verifyOnline(_ref) {
  var realm = _ref.realm,
      authServerUrl = _ref.authServerUrl;
  return function (accessToken) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    return _axios["default"].get("".concat(authServerUrl, "/auth/realms/").concat(options.realm || realm, "/protocol/openid-connect/userinfo"), {
      headers: {
        Authorization: "Bearer ".concat(accessToken)
      }
    }).then((0, _ramda.prop)('data'));
  };
};

var makeUser = function makeUser(_ref2) {
  var sub = _ref2.sub,
      preferred_username = _ref2.preferred_username,
      email_verified = _ref2.email_verified,
      resource_access = _ref2.resource_access,
      email = _ref2.email,
      name = _ref2.name;
  return {
    id: sub,
    userName: preferred_username,
    emailVerified: email_verified,
    resourceAccess: resource_access,
    email: email,
    name: name
  };
};

var userInfo = ['sub', 'preferred_username', 'email_verified', 'resource_access', 'email', 'name'];
var extractUser = (0, _ramda.compose)(makeUser, (0, _ramda.pick)(userInfo));
var verify = (0, _ramda.curryN)(2)(_jsonwebtoken["default"].verify);

var isTheRightKid = function isTheRightKid(kid) {
  return function (publicKey) {
    return publicKey.kid === kid;
  };
};

var findPublicKeyFromKid = function findPublicKeyFromKid(publicKeys) {
  return function (kid) {
    return (0, _ramda.find)(isTheRightKid(kid))(publicKeys);
  };
};

var getKid = (0, _ramda.path)(['header', 'kid']);
var decode = (0, _ramda.compose)((0, _ramda.curryN)(2), _ramda.flip)(_jsonwebtoken["default"].decode);

var getUserFromVerifiedToken = function getUserFromVerifiedToken(token) {
  return function (publicKeys) {
    return (0, _ramda.compose)(extractUser, verify(token), _jwkToPem["default"], findPublicKeyFromKid(publicKeys), getKid, decode({
      complete: true
    }))(token);
  };
};

var fetchPublicKeys = function fetchPublicKeys(_ref3) {
  var realm = _ref3.realm,
      authServerUrl = _ref3.authServerUrl,
      useCache = _ref3.useCache;
  var url = "".concat(authServerUrl, "/auth/realms/").concat(realm, "/protocol/openid-connect/certs");
  var key = url;

  if (useCache) {
    return cache[key] ? Promise.resolve(cache[key]) : _axios["default"].get(url).then((0, _ramda.path)(['data', 'keys'])).then(function (publicKeys) {
      cache[key] = publicKeys;
      return publicKeys;
    });
  } else {
    return _axios["default"].get(url).then((0, _ramda.path)(['data', 'keys']));
  }
};

var verifyOffline = function verifyOffline(config) {
  return (
    /*#__PURE__*/
    function () {
      var _ref4 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(accessToken) {
        var options,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                options = _args.length > 1 && _args[1] !== undefined ? _args[1] : {};
                return _context.abrupt("return", fetchPublicKeys(_objectSpread({}, config, {}, options)).then(getUserFromVerifiedToken(accessToken)));

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref4.apply(this, arguments);
      };
    }()
  );
};

var Keycloak = function Keycloak(config) {
  return {
    verifyOnline: verifyOnline(config),
    verifyOffline: verifyOffline(config)
  };
};

var _default = Keycloak;
exports["default"] = _default;